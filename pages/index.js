import React, { useState, useEffect } from "react";
import fetch from "isomorphic-unfetch";
import Fixtures from "../components/fixtures";
import SubHeader from "../components/layout/subHeader";
import ContentContainer from "../components/layout/contentContainer";
import Fallback from "../components/layout/fallback";

const display = (isLoading, error, fixtures) => {
  switch (true) {
    case isLoading:
      return [...Array(5)].map((item, index) => (
        <Fixtures key={index} data={item} isLoading={true} />
      ));
    case error != null:
      return <Fallback title={"No Matches Found."} />;
    default:
      return fixtures.map((item, index) => (
        <Fixtures key={index} data={item} />
      ));
  }
};

const Index = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [fixtures, setFixtures] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const date = new Date().toJSON().slice(0, 10);
      const result = await fetch(
        `${process.env.API_URL}/api/fixtures?status=1H-HT-2H-ET-P&date=${date}`
      );
      const json = await result.json();
      setIsLoading(false);
      if (result.status != 200 || json.fixtures.length == 0) {
        setError("error");
        return;
      }
      setFixtures(json.fixtures);
    };
    fetchData();
    setInterval(() => {
      fetchData();
    }, 60 * 1000);
  }, []);

  return (
    <React.Fragment>
      <SubHeader
        data={[
          { link: `/`, title: "Live", type: "" },
          { link: `/fixtures`, title: "Fixtures", type: "fixtures" },
          { link: `/results`, title: "Results", type: "results" },
        ]}
        activeTitle={"Live"}
      />
      <ContentContainer displayAd={false}>
        {display(isLoading, error, fixtures)}
      </ContentContainer>
    </React.Fragment>
  );
};

export default Index;
