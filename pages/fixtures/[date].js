import React, { useState, useEffect } from "react";
import fetch from "isomorphic-unfetch";
import Fixtures from "../../components/fixtures";
import SubHeader from "../../components/layout/subHeader";
import ContentContainer from "../../components/layout/contentContainer";
import Fallback from "../../components/layout/fallback";
import DateTimePicker from "../../components/dateTimePicker";
import { useRouter } from "next/router";

/*
 * TODO
 * Create date picker
 * Send another query based on date change
 */

const display = (isLoading, error, fixtures) => {
  switch (true) {
    case isLoading:
      return [...Array(5)].map((item, index) => (
        <Fixtures key={index} data={item} isLoading={true} />
      ));
    case error != null:
      return <Fallback title={"No Matches Found."} />;
    default:
      return fixtures.map((item, index) => (
        <Fixtures key={index} data={item} />
      ));
  }
};

const FixturesPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [fixtures, setFixtures] = useState([]);
  const router = useRouter();

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetch(
        `${process.env.API_URL}/api/fixtures?status=NS&date=${router.query.date}`
      );
      const json = await result.json();
      setIsLoading(false);
      if (result.status != 200 || json.fixtures.length == 0) {
        setError("error");
        return;
      }
      setFixtures(json.fixtures);
    };
    router.query.date !== undefined && fetchData();
  }, [router.query.date]);

  return (
    <React.Fragment>
      <SubHeader
        data={[
          { link: `/`, title: "Live", type: "" },
          { link: `/fixtures`, title: "Fixtures", type: "fixtures" },
          { link: `/results`, title: "Results", type: "results" },
        ]}
        activeTitle={"Fixtures"}
      />
      <ContentContainer displayAd={false}>
        <DateTimePicker
          dateSelected={router.query.date}
          hrefPath={"/fixtures"}
        />
        {display(isLoading, error, fixtures)}
      </ContentContainer>
    </React.Fragment>
  );
};

export default FixturesPage;
