import React from "react";
import fetch from "isomorphic-unfetch";
import LeagueTable from "../../components/leagueTable";
import { useRouter } from "next/router";
import Link from "next/link";
import get from "lodash.get";
import Head from "next/head";

/*
 * TODO
 * Create re-useable dropdown menu
 * Create a fallback display/animation
 * Display error screen
 * Update standings api to return both selected league data and standings
 */
export default function Standings({ standings, leagues, league }) {
  const router = useRouter();
  if (router.isFallback) return <div>{"loading"}</div>;

  return (
    <React.Fragment>
      <Head>
        <title>{`${league.name} ${league.season}, Football ${league.country} - SportingFlash.co.uk`}</title>
        <meta name={"description"} content={`${league.name} ${league.season} table. Find ${league.name} ${league.season} table, home/away standings and ${league.name} ${league.season} last five matches (form) table.`}></meta>
      </Head>
      {leagues &&
        leagues.map((item, index) => (
          <Link key={index} href={`/tables/${item.name_id}`}>
            <a>{item.name}</a>
          </Link>
        ))}
      {standings &&
        standings.map((item, index) => <LeagueTable key={index} data={item} />)}
    </React.Fragment>
  );
}

export const getStaticProps = async (ctx) => {
  const name = ctx.params.name;

  const [standingsData, leagueData, selectedLeague] = await Promise.all([
    fetch(`${process.env.API_URL}/api/standings/${name}`).then((r) => r.json()),
    fetch(`${process.env.API_URL}/api/leagues?standings=1`).then((r) =>
      r.json()
    ),
    fetch(`${process.env.API_URL}/api/leagues?name_id=${name}`).then((r) =>
      r.json()
    ),
  ]);

  return {
    props: {
      standings: get(standingsData, "standings[0].league_table", null) || null,
      leagues: leagueData.leagues || null,
      errors: standingsData.errors || null,
      league: selectedLeague.leagues[0] || null,
    },
  };
};

export const getStaticPaths = async () => {
  const res = await fetch(`${process.env.API_URL}/api/leagues?standings=1`);
  const json = await res.json();
  const paths = json.leagues.map((league) => {
    return { params: { name: league.name_id } };
  });
  return {
    paths,
    fallback: true,
  };
};
