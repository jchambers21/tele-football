import React from "react";
import { useRouter } from "next/router";
import ContentContainer from "../../components/layout/contentContainer";

const League = () => {
  const router = useRouter();
  return (
    <ContentContainer>
      <div>{router.query.name_id}</div>
    </ContentContainer>
  );
};

export default League;
