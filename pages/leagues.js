import React from "react";
import fetch from "isomorphic-unfetch";
import Link from "next/link";
import ContentContainer from "../components/layout/contentContainer";

/*
 * TODO
 * Style page
 */
const Leagues = ({ leagues }) => {
  return (
    <React.Fragment>
      <ContentContainer displayAd={true}>
        {leagues.map((item, index) => (
          <Link key={index} href={`/league/${item.name_id}`}>
            <div>
              <img src={item.logo} />
              <a>{item.name}</a>
            </div>
          </Link>
        ))}
      </ContentContainer>
    </React.Fragment>
  );
};

Leagues.getInitialProps = async ({ query }) => {
  const res = await fetch(`${process.env.API_URL}/api/leagues`);
  const json = await res.json();
  return { leagues: json.leagues, error: json.error, query };
};

export default Leagues;
