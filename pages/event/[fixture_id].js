import React from "react";
import fetch from "isomorphic-unfetch";
import { useRouter } from "next/router";
import ContentContainer from "../../components/layout/contentContainer";

/*
 * Need to update Head to include team names/kick off and other keywords for seo
 * create layout for this componet
 */

export async function getStaticProps({ params }) {
  const result = await fetch(
    `${process.env.API_URL}/api/fixtures?fixture_id=${params.fixture_id}`
  );
  const json = await result.json();

  return {
    props: { event: json.fixtures },
  };
}

export async function getStaticPaths() {
  // fallback: true means that the missing pages
  // will not 404, and instead can render a fallback.
  return { paths: [], fallback: true };
}

const EventFixture = ({ event }) => {
  const router = useRouter();
  if (router.isFallback) {
    return (
      <ContentContainer>
        <div>Loading...</div>
      </ContentContainer>
    );
  }

  return (
    <ContentContainer>
      <div>{router.query.fixture_id}</div>
    </ContentContainer>
  );
};

export default EventFixture;
