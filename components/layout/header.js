import React from "react";
import Link from "next/link";

const Header = () => {
  return (
    <header className={"header"}>
      <Link href={"/"}>
        <a href="" className={"header__logo"}>
          <img src={""} alt={"Companies Logo"} />
        </a>
      </Link>
      <div className={"header__content"}>
        <input
          className={"header__menu-btn"}
          type={"checkbox"}
          id={"menu-btn"}
        />
        <label className={"header__menu-icon"} htmlFor={"menu-btn"}>
          <span className={"header__menu-icon__navicon"} />
        </label>

        <ul className={"header__menu"}>
          <li>
            <Link href={"/"}>
              <a>{"Live"}</a>
            </Link>
          </li>
          <li>
            <Link href={"/leagues"}>
              <a>{"Leagues"}</a>
            </Link>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
