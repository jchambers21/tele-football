import { Fragment, useEffect } from "react";
import Head from "next/head";
import Header from "./header";
import Footer from "./footer";
import { initGA, logPageView } from "../../utils/analytics";

const Layout = (props) => {
  useEffect(() => {
    if (process.env.NODE_ENV === "production") {
      if (!window.GA_INITIALIZED) {
        initGA();
        window.GA_INITIALIZED = true;
      }
      logPageView();
    }
  });

  return (
    <Fragment>
      <Head>
        <title>{"SportingFlash: Live Sporting Events"}</title>
        <meta
          name={"description"}
          content={
            "SportingFlash providers live football scores, fixtures and accurate football results from 1000+ competitions. Follow live sporting results now!"
          }
        />
        <meta
          name={"keywords"}
          content={
            "football scores, live scores, football results, flash scores, football livescore, soccer results, live score, livescores, live football scores, latest results, latest scores, live footy, football fixtures "
          }
        />
        <meta charSet={"UTF-8"} />
        <meta
          name={"viewport"}
          content={"initial-scale=1.0, width=device-width"}
        />
      </Head>
      <Header />
      {props.children}
      <Footer />
    </Fragment>
  );
};

export default Layout;
