import React from "react";
import isNil from "lodash.isnil";

const Fallback = ({ title, image }) => {
  return (
    <div className={"fallback"}>
      {!isNil(image) && <img src={image} />}
      <span>{title}</span>
    </div>
  );
};

export default Fallback;
