import React from "react";
import { useRouter } from "next/router";
import cx from "classnames";

const SubHeader = ({ data, activeTitle }) => {
  const router = useRouter();
  const route = router.asPath;

  return (
    <div className={"subHeader"}>
      {data.map((item, index) => {
        const className = cx({
          subHeader__link: true,
          "subHeader__link--active": activeTitle == item.title,
        });
        return (
          <a
            key={index}
            href={route !== item.link ? item.link : null}
            className={className}
          >
            {item.title}
          </a>
        );
      })}
    </div>
  );
};

export default SubHeader;
