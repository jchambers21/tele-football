import React from "react";
import Link from "next/link";

const Footer = ({settings}) => {
  const d = new Date();
  const currentYear = d.getFullYear();

  return (
    <footer className={"footer"}>
      <div className={"footer__container"}>
        <div className={"header__cta--link"}>
          <a href={"/sitemap.xml"}>{"Sitemap"}</a>
        </div>
        <div className={"header__cta--link"}>
          <Link href={`/Live`}>
            <a>{"Live"}</a>
          </Link>
        </div>
        <div className={"header__cta--link"}>
          <Link href={`/Leagues`}>
            <a>{"Leagues"}</a>
          </Link>
        </div>
      </div>
      <div className={"footer__container"}>
        <span>{`©Copyright ${currentYear}, SportingFlash.co.uk. All rights reserved.`}</span>
      </div>
    </footer>
  );
};

export default Footer;
