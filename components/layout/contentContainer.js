import React from "react";

const ContentContainer = ({ children, displayAd }) => {
  return (
    <div className={"content-container"}>
      {displayAd && <div>{"Ad"}</div>}
      {children}
    </div>
  );
};

export default ContentContainer;
