import React from "react";
import cx from "classnames";

const NavBar = (props) => {
  const { tabTitles, activeTitle, updateTitle } = props;

  return (
    <div className={"navbar"}>
      {tabTitles.map((title, index) => {
        const className = cx({
          navbar__child: true,
          "navbar__child--active": title == activeTitle,
        });
        return (
          <div
            key={index}
            className={className}
            onClick={() => updateTitle(title)}
          >
            {title}
          </div>
        );
      })}
    </div>
  );
};

export default NavBar;
