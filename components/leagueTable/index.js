import { Component } from "react";

class LeagueTable extends Component {
  render() {
    const { data } = this.props;
    return (
      <table>
        <thead>
          <tr>
            <th>#</th>
            <th>Team</th>
            <th>GP</th>
            <th>W</th>
            <th>D</th>
            <th>L</th>
            <th>GD</th>
            <th>PTS</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => {
            return (
              <tr key={index}>
                <td>{item.rank}</td>
                <td>{item.teamName}</td>
                <td>{item.all.matchsPlayed}</td>
                <td>{item.all.win}</td>
                <td>{item.all.draw}</td>
                <td>{item.all.lose}</td>
                <td>{item.goalsDiff}</td>
                <td>{item.points}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default LeagueTable;
