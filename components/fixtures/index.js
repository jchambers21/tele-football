import React, { useState } from "react";
import SelectedFixture from "./selectedFixture";
import get from "lodash.get";
import cx from "classnames";

const Fixtures = ({ data, isLoading }) => {
  const [displayFixtures, setDisplayFixtures] = useState(true);

  const containerClass = cx({
    fixtures: true,
    "fixtures--loading": isLoading,
  });

  return (
    <div className={containerClass}>
      <div className={"fixtures__banner"}>
        <div className={"fixtures__banner__data"}>
          {get(data, "league", null) != null && (
            <div>{`${data.league.country}: ${data.league.name}`}</div>
          )}
        </div>

        <div
          className={"fixtures__banner__toggle"}
          onClick={() => setDisplayFixtures(!displayFixtures)}
        >
          {displayFixtures ? "Hide" : "Show"}
        </div>
      </div>

      {isLoading
        ? [...Array(5)].map((item, index) => (
            <SelectedFixture key={index} data={item} isLoading={true} />
          ))
        : displayFixtures &&
          data.fixtures != undefined &&
          data.fixtures.map((item, index) => (
            <SelectedFixture key={index} data={item} />
          ))}
    </div>
  );
};

export default Fixtures;
