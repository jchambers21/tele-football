import React from "react";
import Link from "next/link";

const getTime = (data) => {
  switch (data.statusShort.toLowerCase()) {
    case "p":
      return "Pen";
    case "ns":
      return data.event_date.split("T")[1].replace("Z", "").substring(0, 5);
    case "ft":
    case "ht":
      return data.statusShort;
    default:
      return data.elapsed + "`";
  }
};

const getScore = (data) => {
  switch (data.statusShort.toLowerCase()) {
    case "ns":
      return ":";
    default:
      return `${data.goalsHomeTeam} : ${data.goalsAwayTeam}`;
  }
};

const SelectedFixture = ({ data, isLoading }) => {
  return (
    <React.Fragment>
      {isLoading ? (
        <div className={"selectedFixture"}>
          <div className={"selectedFixture__time"}>
            <div />
          </div>
          <div className={"selectedFixture__team"}>
            <div />
          </div>
          <div className={"selectedFixture__score"}>
            <div />
          </div>
          <div className={"selectedFixture__team"}>
            <div />
          </div>
        </div>
      ) : (
        <Link href={`/event/${data.fixture_id}`}>
          <div className={"selectedFixture"}>
            <div className={"selectedFixture__time"}>{getTime(data)}</div>
            <div className={"selectedFixture__team"}>
              {data.homeTeam.team_name}
            </div>
            <div className={"selectedFixture__score"}>{getScore(data)}</div>
            <div className={"selectedFixture__team"}>
              {data.awayTeam.team_name}
            </div>
          </div>
        </Link>
      )}
    </React.Fragment>
  );
};

export default SelectedFixture;
