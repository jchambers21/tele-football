import React, { useEffect, useState } from "react";
import cx from "classnames";
import Link from "next/link";

const GetDates = (startDate, daysToAdd) => {
  const aryDates = [];

  for (let i = 0; i <= daysToAdd; i++) {
    const currentDate = new Date();
    currentDate.setDate(startDate.getDate() + i);

    aryDates.push({
      title: `${DayAsString(currentDate.getDay())} ${currentDate.getDate()}`,
      date: `${currentDate.getFullYear()}-${addZ(
        currentDate.getMonth() + 1
      )}-${addZ(currentDate.getDate())}`,
    });
  }

  return aryDates;
};

const DayAsString = (dayIndex) => {
  const weekdays = new Array(7);
  weekdays[0] = "Sun";
  weekdays[1] = "Mon";
  weekdays[2] = "Tue";
  weekdays[3] = "Wed";
  weekdays[4] = "Thu";
  weekdays[5] = "Fri";
  weekdays[6] = "Sat";

  return weekdays[dayIndex];
};

const addZ = (n) => {
  return n < 10 ? "0" + n : "" + n;
};

const DateTimePicker = ({ displayPastDate, dateSelected = null, hrefPath }) => {
  const [dates, setDates] = useState([]);

  useEffect(() => {
    const startDate = displayPastDate
      ? new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
      : new Date();
    const aryDates = GetDates(startDate, 7);
    if (displayPastDate) aryDates.reverse();
    setDates(aryDates);
  }, []);

  return (
    <div className={"dateTime"}>
      {dates.map((date, index) => {
        const className = cx({
          dateTime__item: true,
          "dateTime__item--selected":
            date.date == dateSelected || (index == 0) & (dateSelected == null),
        });
        return (
          <Link key={index} href={`${hrefPath}/${date.date}`}>
            <div className={className}>{index == 0 ? "Today" : date.title}</div>
          </Link>
        );
      })}
    </div>
  );
};

export default DateTimePicker;
